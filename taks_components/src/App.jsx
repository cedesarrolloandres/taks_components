import Login from './components/login.jsx';
import Portada from './components/portada.jsx';
import Contacto from './components/contacto.jsx';
import Information from './components/information.jsx';
function App() {
  return (
    <div>
      <Portada/>
      <Login/>
      <Contacto/>
      <Information/>
    </div>
  );
}

export default App;
